package com.craigb.test;

import java.io.PrintStream;

/**
 * Created by Craigb on 14/12/2016.
 */
public class SystemLogger {

  public static void dumpProperties(PrintStream out) {
    System.getProperties().list(out);
  }

  public void sayHello() {
    System.out.println("Hello");
  }
}
